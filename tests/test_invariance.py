import os

import joblib
import numpy as np
import pandas as pd
import pytest
from sklearn.metrics import accuracy_score

dir_path = os.path.dirname(os.path.realpath(__file__))


def fare_minus(x, diff):
    if x >= 100:
        return x - 100
    else:
        return x


@pytest.fixture
def model():
    path = dir_path + "/../models/titanic.pkl"
    model = joblib.load(path)  # pytest metafunc
    yield model
    assert os.path.exists(path)  # assert we did not remove it


@pytest.fixture
def dataset():
    path = dir_path + "/../data/test.csv"
    df = pd.read_csv(path, index_col="PassengerId")
    yield df.drop("Survived", axis=1), df["Survived"]
    assert os.path.exists(path)


def test_invariance_1(dataset, model):
    X, y_true = dataset
    X['Age'] = X['Age'] * 0.8
    y_pred = model.predict(X)
    assert accuracy_score(y_pred, y_true) > 0.6


def test_invariance_2(dataset, model):
    X, y_true = dataset
    X['Fare'] = X['Fare'].apply(lambda x: fare_minus(x, 100))
    y_pred = model.predict(X)
    assert accuracy_score(y_pred, y_true) > 0.6


def test_invariance_3(dataset, model):
    X, y_true = dataset
    X['Pclass'] = np.random.randint(1, 4, len(X['Pclass']))
    y_pred = model.predict(X)
    assert accuracy_score(y_pred, y_true) > 0.6
